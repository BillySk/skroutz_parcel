import React from 'react';
import '../App.css';
import { searchGiphies } from '../gifies';
import {getGiphiesSuccess, getGiphiesError, getPagination} from '../redux/reducers';
import { connect } from 'react-redux';
import {fetchGiphiesErrorAction, fetchGiphiesSuccessAction} from '../redux/actions';
import { bindActionCreators } from 'redux';

class GridButtons extends React.Component {
    constructor(props) {
      super(props);
      this.state= {
          pagination: Object.keys(this.props.pagination).length > 0 ? this.props.pagination : JSON.parse(localStorage.getItem("giphies.pagination")),
          name: this.props.name,
      }
    }

    render() {
      return (
        (this.state.pagination && Object.keys(this.state.pagination).length !== 0) ?
        <div className="buttons">
            <button 
                disabled={this.state.pagination.offset === 0}
                onClick={(e) => this.goToPage(e, 0)}>
                first
            </button>
            <button
                disabled={this.state.pagination.offset === 0}
                onClick={(e) => this.goToPage(e, this.state.pagination.offset - this.state.pagination.count)}>
                prev
            </button>
            <button
                disabled={this.state.pagination.offset + this.state.pagination.count >= this.state.pagination.total_count}
                onClick={(e) => this.goToPage(e, this.state.pagination.count + this.state.pagination.offset)}
                >
                next
            </button>
            {/* Giphie api is not working well with big offset so I removed last button */}
            {/* <button
                disabled={this.state.pagination.offset + this.state.pagination.count >= this.state.pagination.total_count}
                onClick={(e) => this.goToPage(e, this.state.pagination.total_count - (this.state.pagination.total_count % 25) !== 0 ?
                                                this.state.pagination.total_count - (this.state.pagination.total_count % 25) :
                                                this.state.pagination.total_count - this.state.pagination.count)}
                >
                last
            </button> */}
        </div>
        : null
      );
    }

    goToPage(e, offset) {
        const params={
            giphies: new Map(),
            name: this.state.name,
            actions: {
                success: this.props.fetchGiphiesSuccessAction,
                error: this.props.fetchGiphiesErrorAction
            },
            offset: offset        
        };
        searchGiphies(e, params);
    }

    static getDerivedStateFromProps (props){
        return {
            giphies: props.giphies,
            name: props.name,
            pagination: props.pagination && Object.keys(props.pagination).length > 0 ? props.pagination : JSON.parse(localStorage.getItem("giphies.pagination"))
        }
    }
}


const mapStateToProps = state => ({
    error: getGiphiesError(state),
    giphies: getGiphiesSuccess(state),
    pagination: getPagination(state)
})

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchGiphiesErrorAction,
    fetchGiphiesSuccessAction,
}, dispatch)
  
export default  connect(
    mapStateToProps,
    mapDispatchToProps
)(GridButtons);