import React from 'react';
import '../App.css';
import Gallery from 'react-grid-gallery';


class Grid extends React.Component {
    constructor(props) {
      super(props);
      this.state= {
          giphies: this.props.giphies,
          name: this.props.name,
          notReload: false,
          selectedImages: false
      }
      this.deleteImages = this.deleteImages.bind(this, true);
    }
    
    filterImages() {
        let value = this.state.giphies.get(this.state.name);
        if(value) {
            value.map((gif) => {
                gif.thumbnailWidth = Number(gif.images.original.width);
                gif.thumbnailHeight = Number(gif.images.original.height);
                gif.src = gif.images.downsized.url;
                gif.thumbnail = gif.images.downsized.url;
                gif.customOverlay = (
                    <button style={{position: "absolute", bottom: 0, left: 0}}>Copy to cliboard</button>
                );
                return gif;
            });
            return value;
        }
    }

    onSelectImage (index) {        
        let images = [];
        images = this.state.giphies.get(this.state.name);
        let selectedImages = false;
        if(images[index].hasOwnProperty("isSelected")) {
            images[index].isSelected = !images[index].isSelected;
        } else {
            images[index].isSelected = true;
        }
        if(images[index].isSelected) {
            selectedImages = true;
        }
        let giphies = new Map();
        giphies.set(this.state.name, images);
        this.setState({
            giphies: giphies,
            notReload: false,
            selectedImages: selectedImages
        });
    }
    
    copyToCliboard (e) {
        navigator.clipboard.writeText(this.state.giphies.get(this.state.name)[e].images.original.url);
    }

    deleteImages() {
        let images = [];
        images = this.state.giphies.get(this.state.name).slice();
        for (let i=0; i < images.length; i++ ) {
            if(images[i].isSelected && images[i].isSelected === true) {
                images.splice(i,1);
                i--;
            }
        }
        this.state.giphies.set(this.state.name, images)
        this.setState({
            giphies: this.state.giphies,
            notReload: false,
            selectedImages: false
        });

    }

    render() {
      return (
        (this.state.giphies && this.state.giphies.size > 0) &&
        <div>
            <button disabled={!this.state.selectedImages}  onClick={this.deleteImages} className="deleteButton">Delete selected images</button>
            <Gallery
                images={this.filterImages() || []}
                enableLightbox={false}
                onSelectImage={this.onSelectImage.bind(this)}
                onClickThumbnail={(e) => this.copyToCliboard(e)}
            />
        </div>
      );
    }

    static getDerivedStateFromProps (props){
        return {
            giphies: props.giphies,
            name: props.name
        }
    }

    // componentDidMount () {
    //     if(!this.state.giphies) {
    //         return;
    //     }
    //     let value = this.state.giphies.get(this.state.name);
    //     if(value) {
    //         value.map((gif) => {
    //             gif.thumbnailWidth = Number(gif.images.original.width);
    //             gif.thumbnailHeight = Number(gif.images.original.height);
    //             gif.src = gif.images.original.url;
    //             gif.thumbnail = gif.images.original.url;
    //             gif.customOverlay = (
    //                 <button style={{position: "absolute", bottom: 0, left: 0}}>Copy to cliboard</button>
    //             );
    //             return gif;
    //         })
    //         let giphies= this.state.giphies;
    //         this.setState({
    //             giphies: giphies
    //         });
    //     }
    // }

    shouldComponentUpdate(nextProps, nextState) { 
        if (nextProps.giphies.size === this.state.giphies.size && nextState.notReload) {
            return false 
        } else {
            return true;
        } 
      }
}

  
export default Grid;