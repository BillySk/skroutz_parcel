const JSON = require('circular-json');
const api_key = "qcxTWYJ06UYFe1G4U7BxIOPm5HWTosax";

export const searchGiphies = (e, params) => {
    e.preventDefault();
    if(!params.giphies.get(params.name) || !params.offset) {
      fetchGiphies(params.name, params.actions.success, params.actions.error, params.offset);
    } else {
        params.actions.success(params.name, params.giphies.get(params.name), params.offset);
    }
}

export const getGiphies = (name, giphies) => {
    const giphiesTemp= new Map();
    if (localStorage.getItem("giphies.images") && !name){
        giphiesTemp.set(getName(name), typeof localStorage.getItem("giphies.images") === "string" ?JSON.parse(localStorage.getItem("giphies.images")): localStorage.getItem("giphies.images"));
        return giphiesTemp;
    }
    return giphies;
}

export const setGiphies = (name, giphies, pagination) => {
    localStorage.setItem("giphies.name", name);
    localStorage.setItem("giphies.images", JSON.stringify(giphies));
    localStorage.setItem("giphies.pagination", JSON.stringify(pagination));
}

export const getName = (name) => {
    return name || localStorage.getItem("giphies.name");
}

export const fetchGiphies = (name, fetchGiphiesSuccessAction, fetchGiphiesErrorAction, offset) => {
    let url;
    if(offset) {
        url = 'https://api.giphy.com/v1/gifs/search?api_key='+ api_key +'&q='+name+ '&offset='+ offset;
    } else {
        url = 'https://api.giphy.com/v1/gifs/search?api_key='+ api_key +'&q='+name;
    }
    fetch(url)
    .then(res => res.json())
    .then(res => {
        if(res.error) {
            throw(res.error);
        }
        // let data = map; 
        // data[name] = res.data;
        fetchGiphiesSuccessAction(name, res.data, res.pagination);
        return res.data;
    })
    .catch(error => {
        fetchGiphiesErrorAction(error);
    })
}