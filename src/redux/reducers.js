import { FETCH_GIPHIES_ERROR, FETCH_GIPHIES_SUCCESS} from './actions';
import { setGiphies } from '../gifies';

export const initialState = {
    giphies: new Map(),
    limit: 0,
    error: null,
    pagination: {}
}

const fetchGiphiesReducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_GIPHIES_SUCCESS:
            setGiphies(action.name, action.giphies, action.pagination);
            state.giphies.set( action.name, action.giphies);
            const stateGiphies = new Map(state.giphies);
            return {
                giphies: stateGiphies,
                pagination: action.pagination
            }
        case FETCH_GIPHIES_ERROR:
            return {
                error: action.error
            }
        default:
            return state;
    }
}

export const getGiphiesSuccess = state => state.giphies;
export const getGiphiesError = state => state.error;
export const getPagination = state => state.pagination;
export default fetchGiphiesReducer;