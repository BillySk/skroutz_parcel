import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import fetchGiphiesReducer from './redux/reducers';

const store = createStore(fetchGiphiesReducer);

class Root extends React.Component {
    render() {
        return (
        <Provider store={store}>
            <App />
        </Provider>
        );
    }
}

let Root2 = document.getElementById("app");

ReactDOM.render(<Root name="root" />, Root2);
