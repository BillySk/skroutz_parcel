import React, {Component} from 'react';
import logo from './assets/giphy.png';
import {searchGiphies, getGiphies, getName} from './gifies';
import {fetchGiphiesErrorAction, fetchGiphiesSuccessAction} from './redux/actions';
// import fetchGiphiesAction from './fetchGiphies';
import {getGiphiesSuccess, getGiphiesError} from './redux/reducers';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './App.css';
import Grid from './components/Grid';
import GridButtons from './components/GridButtons';

class App extends Component {
  constructor(props) {
    super(props);

    this.state= {
      name: getName(this.props.name) || "",
      giphies: getGiphies(this.props.name, this.props.giphies)
    }
  }

  render() {
    const searchGiphiesParams= { 
      giphies: this.props.giphies,
      name: this.state.name,
      actions: {
        success: this.props.fetchGiphiesSuccessAction,
        error: this.props.fetchGiphiesErrorAction
      },
    };
    return (
        <div id="root" className="App">
          <img src={logo} alt="logo" className="logoImage"/>
          <div className="searchBar">
            <label>
              Search:  
            </label>
            <input type="text" className="input" name="name" value={this.state.name} onChange={(e) => this.setState({name: e.target.value})}/>
            <button onClick={(e) => searchGiphies(e, searchGiphiesParams)}>Search</button>
          </div>
          <Grid giphies={this.props.giphies.size ? this.props.giphies : this.state.giphies}
                name={getName(this.props.name)}
                fetchGiphiesSuccess={this.props.fetchGiphiesSuccess}
                fetchGiphiesError={this.props.fetchGiphiesError}
          />
          <GridButtons
                pagination={this.props.pagination}
                name={this.state.name}
            />
        </div>
    );
  }

}

const mapStateToProps = state => ({
  error: getGiphiesError(state),
  giphies: getGiphiesSuccess(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchGiphiesErrorAction,
  fetchGiphiesSuccessAction,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
