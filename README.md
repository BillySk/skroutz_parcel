# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Giphy app ###

* Quick summary
A user interface for the giphy api with delete, copy to cliboard and simple pagination functionalities.
This implementation includes memory caching of the user's searches and localStorage caching for every last search of the user before refreshing the page.
Redux used for state management.


### Installing and running ###

* Installing node dependencies
Run: npm install

* Run on development mode
Run: npm start

* Build application
Run: npm run build

### Who do I talk to? ###

* Vasilis Skouras (skourasbl@gmail.com)